@extends('layout.master')

@section('content')

		<h1>Edit</h1>
		<div class="row">
			<div class="col-lg-12">
			 <!--FORMULIR -->
			 <form action="/siswa/{{$siswa->id}}/update" method="POST">
					        	{{csrf_field()}}
						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Lengkap</label>
						    <input name="nama_lengkap" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{$siswa->nama_lengkap}}">
						    </div>

						    <div class="form-group">
						    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
						    <select class="form-control" id="exampleFormControlSelect1" name="jenis_kelamin">
						      <option value="L" @if($siswa->jenis_kelamin == 'L') selected @endif>Laki-Laki</option>
						      <option value="P" @if($siswa->jenis_kelamin == 'P') selected @endif>Perempuan</option>
						       </select>
							 </div>

							 <div class="form-group">
						    <label for="exampleInputEmail1">Agama</label>
						    <input  name="agama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama" value="{{$siswa->agama}}">
						    </div>

						  <div class="form-group">
						    <label for="exampleInputPassword1">Alamat</label>
						     <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat">{{$siswa->alamat}}</textarea>
						  </div>	  
											      
						</div>
					      <div class="modal-footer">
					        <button type="submit" class="btn btn-primary">Update</button>
						</form>
						</div>
			
</div>

	@endsection