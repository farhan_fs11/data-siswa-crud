<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'project';
    protected $fillable = ['nama_lengkap','jenis_kelamin','agama','alamat'];
}