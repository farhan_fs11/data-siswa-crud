<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('/siswa','SiswaController@index');

route::post('/siswa/create','SiswaController@create');
route::get('/siswa/{id}/edit','SiswaController@edit');
route::post('/siswa/{id}/update','SiswaController@update');
route::get('/siswa/{id}/delete','SiswaController@delete');